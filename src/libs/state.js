import React, { createContext, useContext, useReducer } from 'react'

export const StateContext = createContext()

export const StateProvider = ({ reducer = (state) => state, initialState, children }) => (
  <StateContext.Provider value={useReducer(reducer, initialState)}>
    {children}
  </StateContext.Provider>
)

export const useGlobalState = () => useContext(StateContext)

export const initialState = {
  bookForm: {
    id: '',
    name: '',
    rideDate: new Date(),
    time: {
      hour: 8,
      min: 0,
      meridian: 'AM'
    },
    pricing: '1 hour',
    selectingDate: false,
  },
  currentUser: null
}

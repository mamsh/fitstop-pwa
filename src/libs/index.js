export const isAdmin = (user) => user && user.role === "admin"

export const getUserFromAuthState = (authState, data) => authState.user ? data && data.user && data.user[0] : null

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December'
]

export const displayDate = (date, shorten) => `${shorten ? months[date.getMonth()].substr(0, 3) : months[date.getMonth()]} ${date.getDate()}, ${date.getFullYear()}`

export const displayTime = (time) => {
  const hour = +time.split(':')[0]
  const min = +time.split(':')[1]
  const diff = hour - 12

  return diff > 0 
    ? `${(diff + '').padStart(2, '0')}:${(min + '').padStart(2, '0')}PM` 
    : `${(hour + '').padStart(2, '0')}:${(min + '').padStart(2, '0')}AM`
}

displayTime("13:00:00")
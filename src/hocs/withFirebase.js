import { FirebaseContext } from '../libs/firebase'
import React from 'react'

const withFirebase = (WrappedComponent) => (
  (props) => (
    <FirebaseContext.Consumer>
      {(firebase) => <WrappedComponent firebase={firebase} {...props} />}
    </FirebaseContext.Consumer>
  )
)

export default withFirebase

import React from 'react'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'

import Icon from '../components/Icon'

const Navigation = ({ user, history }) => (
  <Wrapper>
    <NavigationItem onClick={() => history.push('/shop')}>
      <Icon name='store' /> Shop
    </NavigationItem>
    {
      user && user.role === 'admin' && 
      <NavigationItem onClick={ () => history.push('/bookings')}>
        <Icon name='description' /> Bookings
      </NavigationItem>
    }
    {
      !user && 
      <NavigationItem onClick={ () => history.push('/sign-in') }>
        <Icon name='account_circle' /> Sign in
      </NavigationItem>
    }
    {
      user && 
      <NavigationItem>
        <Icon name='account_circle' /> 
        <Name>{user.first_name}</Name>
      </NavigationItem>
    }
    { user && 
      <NavigationItem onClick={ () => history.push('/sign-in') }>
        Sign out
      </NavigationItem>
    }
  </Wrapper>
)

const Name = styled.div`
  max-width: 80px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`

const Wrapper = styled.div`
  grid-area: navigation;
  display: flex;
  align-items: center;
  justify-content: flex-end;
  padding-right: 100px;
  background: #222222;

  @media (max-width: 680px) {
    justify-content: center;
    padding: 0;
  }
`

const NavigationItem = styled.div`
  color: #FBE9E7;
  display: flex;
  font-size: 14px;
  opacity: 1;
  align-items: center;
  transition: 500ms;
  white-space: nowrap;
  cursor: pointer;
  i {
    font-size: 18px;
    margin-right: 4px;
  }
  :not(:last-child) {
    margin-right: 20px;
  }
`

export default withRouter(Navigation)

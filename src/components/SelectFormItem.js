import React, { useState } from 'react'
import styled from 'styled-components'

import Select from './Select'

const SelectFormItem = ({ value, options, label, onChange, placeholder }) => {
  const [open, setOpen] = useState(false)

  const toggle = (newOpen) => {
    setOpen(newOpen || !open)
  }

  return (
    <FormItem>
      {label && <Label>{label}</Label>}
      <Input
        value={value}
        placeholder={placeholder}
        readOnly
        onClick={() => toggle()}
      />
      {open && <Select 
        selectedValue={value}
        options={options}
        onChange={onChange}
        toggle={toggle} 
      />}
    </FormItem>
  )
}

const Label = styled.div`
  font-size: 14px;
  white-space: nowrap;
  color: #EEEEEE;
  opacity: 0.5;
  z-index: 1001;
`

const Input = styled.input`
  font-size: 14px;
  padding-left: 10px;
  background: none;
  border: none;
  margin: 0;
  color: #EEEEEE;
  width: 100%;
  font-family: 'Lexend Deca', sans-serif;
  white-space: nowrap;
  cursor: pointer;
  z-index: 1001;
  text-transform: capitalize;
`

const FormItem = styled.div`
  display: flex;
  background: #000000;
  position: relative;
  height: 40px;
  border-radius: 20px;
  align-items: center;
  padding-left: 20px;
  padding-right: 20px;
  :not(:first-child) {
    margin-top: 20px;
  }
`

export default SelectFormItem

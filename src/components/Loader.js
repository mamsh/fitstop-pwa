import React from 'react'
import styled from 'styled-components'

import wheel from '../assets/wheel.svg'

const Loader = () => (
  <Wrapper>
    <img src={wheel} alt='Loading' />
  </Wrapper>
)

const Wrapper = styled.div`
  position: absolute;
  z-index: 1000;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

export default Loader

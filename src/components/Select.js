import React from 'react'
import styled from 'styled-components'

const Select = ({ selectedValue, options, onChange, toggle }) => (
  <Wrapper>
    {
      options.map((field) => (
        <Option 
          key={field}
          selected={selectedValue === field}
          onClick={() => {
            onChange(field)
            toggle(false)
          }}
        >{field}</Option>
      ))
    }
  </Wrapper>
)

const Option = styled.div`
  font-size: 14px;
  height: 30px;
  text-transform: capitalize;
  display: flex;
  align-items: center;
  padding: 0 20px;
  opacity: ${({ selected }) => selected ? 1 : 0.5};
  cursor: pointer;
  :hover {
    opacity: 1;
  }
`

const Wrapper = styled.div`
  position: absolute;
  background: #000000;
  margin-top: 20px;
  padding-top: 15px;
  padding-bottom: 10px;
  top: 0;
  right:  0;
  left: 0;
  color: white;
  z-index: 1000;
  border-bottom-left-radius: 20px;
  border-bottom-right-radius: 20px;
`

export default Select

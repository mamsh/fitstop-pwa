import React from 'react'
import styled from 'styled-components'

const Tip = ({ message, arrow, type }) => (
  <Wrapper arrow={arrow} type={type} >
    {arrow !== 'none' && <Triangle />}
    {message}
  </Wrapper>
)

const ARROW_STYLES = {
  'none': {
    marginBottom: 0,
  }
}

const TYPE_STYLES = {
  'error': {
    background: '#D32F2F',
  },
  'success': {
    background: '#3949AB',
  }
}

const Wrapper = styled.div`
  color: #FBE9E7;
  align-self: center;
  display: flex;
  justify-content: center;
  position: relative;
  border-radius: 16px;
  font-size: 12px;
  padding: 16px;
  margin-top: -6px;
  margin-bottom: -10px;
  padding-top: 8px;
  padding-bottom: 8px;
  z-index: 100;
  ${({ arrow }) => ARROW_STYLES[arrow]}
  ${({ type }) => TYPE_STYLES[type]}
  animation: Bounce cubic-bezier(0.445, 0.05, 0.55, 0.95) both 500ms;
`

const Triangle = styled.div`
  height: 12px;
  width: 12px;
  transform: rotate(45deg);
  background: #D32F2F;
  position: absolute;
  top: -6px;
  margin: auto;
`

export default Tip

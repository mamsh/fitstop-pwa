import React from 'react'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'

import logo from '../assets/fs-logo.png'

const Header = ({ history }) => (
  <Wrapper>
    <Logo src={logo} onClick={() => history.push('')} />
  </Wrapper>
)

const Wrapper = styled.div`
  grid-area: header;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding-left: 100px;
  background: #222222;

  @media (max-width: 680px) {
    justify-content: center;
    padding: 0;
  }
`

const Logo = styled.img`
  height: 30px;
  cursor: pointer;
`

export default withRouter(Header)
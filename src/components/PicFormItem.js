import React from 'react'
import styled from 'styled-components'
import Icon from './Icon'

const processImage = (event, onChange, onError) => {
  const image = new Image()

  image.onload = function () {
    document.getElementById('imageFile').src = image.src
    var canvas = document.createElement('canvas')
    var context = canvas.getContext('2d')

    if (image.width < 400 || image.height < 400) {
      onError('Your image is too small!')
    } else {
      canvas.width = 400 * image.width / image.height
      canvas.height = 400
      context.drawImage(image,
        0,
        0,
        image.width,
        image.height,
        0,
        0,
        canvas.width,
        canvas.height
      )
      onChange(canvas.toDataURL())
    }
  }
  image.src = event.target.result
}

const PicFormItem = ({ value, onChange, onError }) => {
  return (
    <FormItem style={{ backgroundImage: `url(${value})`, backgroundSize: 'cover', backgroundPosition: 'center' }}>
      <PicWrapper>
        <Icon name='add_a_photo' />
      </PicWrapper>
      <FileInput
        id='imageFile'
        type='file'
        onChange={(event) => {
          const file = event.target.files[0]
          if (file) {
            const isImage = file.type.startsWith('image/')
            if (!isImage) {
              onError('Please pick an image.')
            }
            if (isImage) {
              const reader = new FileReader()
              reader.addEventListener('load', (e) => {
                processImage(e, onChange, onError)
              })
              reader.readAsDataURL(file)
            }
          }
        }}
      />
      {value && <Remove onClick={() => onChange('')}>
        <Icon name='cancel' />
      </Remove>}
    </FormItem>
  )
}

const Remove = styled.div`
  position: absolute;
  z-index: 200;
  cursor: pointer;
  color: #EEEEEE;
  top: 10px;
  left: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
  filter: invert();
  font-size: 24px;
`

const FileInput = styled.input`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  z-index: 100;
  opacity: 0;
  cursor: pointer;
`

const PicWrapper = styled.div`
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  filter: invert();
  i {
    font-size: 24px;
    color: #EEE;
    cursor: pointer;
    z-index: 1000;
  }
`

const FormItem = styled.div`
  background: #000000;
  position: relative;
  height: 0;
  padding-bottom: 100%;
  border-radius: 20px;
  margin-top: 20px;
`

export default PicFormItem

import React from 'react'
import styled from 'styled-components'

const Button = ({ text, onClick, type, disabled, disabledText }) => (
  <Wrapper type={type} onClick={!disabled && onClick} disabled={disabled}>
    {disabled ? disabledText || 'Submitting' : text}
  </Wrapper>
)

const Wrapper = styled.div`
  font-size: 14px;
  white-space: nowrap;
  height: 40px;
  border-radius: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: 20px;
  padding-right: 20px;
  background: #FBE9E7;
  cursor: pointer;
  ${({ type }) => TYPE_STYLES[type]}
  ${({ disabled }) => disabled && (`
    opacity: 0.2;
    cursor: not-allowed;
  `)}
`

const TYPE_STYLES = {
  'primary': {
    background: '#FFC107',
    color: '#212121',
  },
  'landing': {
    background: '#FFC107',
    color: '#212121',
    fontSize: '16px'
  }
}

export default Button

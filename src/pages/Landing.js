import React from 'react'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'

import Button from '../components/Button'
import LandingGif from '../assets/landing-bg.gif'

const Landing = ({ history }) => (
  <>
    <Background
      style={{ backgroundImage: `url(${LandingGif})`, backgroundSize: 'cover', }}
    />
    <Wrapper>
      <Button
        text='Go for a ride today!'
        type='landing'
        onClick={() => history.push('shop')}
      />
    </Wrapper>
  </>
)

const Background = styled.div`
  position: fixed;
  top: 60px;
  bottom: 0;
  left: 0;
  right: 0;
`

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 400px;
  position: fixed;
  opacity: ${({ isSubmitting }) => isSubmitting ? 0.5 : 1};
  height: calc(100% - 60px); 
  justify-content: center;
  left: calc(50% - 240px);
  padding: 0 40px;
  @media (max-width: 400px) {
    width: 90%;
    left: 5%;
    padding: 0;
  }
`

export default withRouter(Landing)

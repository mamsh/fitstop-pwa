import React, { useState, useEffect } from 'react'
import styled from 'styled-components'
import gql from 'graphql-tag'
import { useSubscription } from '@apollo/react-hooks'
import { withRouter } from 'react-router-dom'
import { useGlobalState } from '../../libs/state'
import { isAdmin } from '../../libs'

import Icon from '../../components/Icon'
import Loader from '../../components/Loader'
import Button from '../../components/Button'
import Item from './Item'

const FETCH_ITEMS = gql`
  subscription fetchItems($filter: item_bool_exp, $limit: Int!, $offset: Int!) {
    results: item(limit: $limit, offset: $offset, where: $filter, order_by: { created_at: desc }) {
      id
    }
  }
`

const Search = ({ history, searching, toggleSearch }) => {
  // set limit per load here
  const limit = 3 
  const [keyword, setKeyword] = useState('')
  const [offset, setOffset] = useState(0)
  const [items, setItems] = useState([])
  const [{ currentUser }] = useGlobalState()
  const defaultFilter = {
    name: {
      _ilike: `%${keyword}%`
    },
  }
  const { data, loading } = useSubscription(FETCH_ITEMS, {
    variables: {
      filter: !isAdmin(currentUser)
        ? {
          ...defaultFilter,
          status: {
            _eq: 'available',
          },
        } : defaultFilter,
      limit,
      offset,
    }
  })

  useEffect(() => {
    // setting items for the first time
    if (items.length === 0 && data) {
      setItems([...items, ...data.results])
    }
    // add new fetched items to list
    if (!loading) {
      setItems([...items, ...data.results])
    }
  }, [data, loading])

  const loadMoreItems = () => {
    setOffset(offset + limit)
  }

  return (
    <>
      <Wrapper searching={searching} onScroll={(e) => {
        const scrolledElement = e.target
        if (scrolledElement.offsetHeight + scrolledElement.scrollTop >= scrolledElement.scrollHeight) {
          loadMoreItems()
        }
      }}>
        <SearchWrapper>
          <SearchInput>
            <IconWrapper><Icon name='search' /></IconWrapper>
            <Input
              type='text'
              value={keyword}
              placeholder='Find a bike'
              onChange={(e) => {
                setKeyword(e.target.value)
                setItems([])
              }}
            />
            <IconWrapper onClick={() => {
              setKeyword('')
              setItems([])
            }}>
              <Icon name='clear' />
            </IconWrapper>
          </SearchInput>
          {isAdmin(currentUser) && <Button style={{ marginLeft: '10px' }} text='New item' onClick={() => history.push('new-item')} />}
        </SearchWrapper>
        <ResultsWrapper>
          {
            items.length
            ? items.map((item, index) => (
              <Item key={index} index={index} id={item.id} toggleSearch={toggleSearch} />
            )) 
            : <EmptyWrapper>{loading ? 'Searching' : 'No results.'}</EmptyWrapper>
          }
          <ButtonWrapper>
          {!!items.length && <Button text='More' onClick={loadMoreItems} disabled={loading} disabledText='Loading' />}
          </ButtonWrapper>
          {!!!items.length && loading && <Loader /> }
        </ResultsWrapper>
      </Wrapper>
    </>
  )
}

const ButtonWrapper = styled.div`
  margin: 10px;
  padding-bottom: 20px;
  display: flex;
  width: 100%;
  float: left;
`

const EmptyWrapper = styled.div`
  color: #EEEEEE;
  padding: 10px;
  opacity: 0.5;
`

const ResultsWrapper = styled.div`
  padding: 30px;
  padding-top: 0;
  display: block;
  position: relative;
`

const SearchWrapper = styled.div`
  padding: 40px;
  padding-bottom: 20px;
  position: sticky;
  background: #111;
  display: flex;
  z-index: 100;
  top: 0;
  right: 0;
  left: 0;

  div:not(:first-child) {
    margin-left: 10px;
  }
`

const IconWrapper = styled.div`
  color: #EEEEEE;
  padding: 0;
  display: flex;
  align-items: center;
  font-size: 18px;
  cursor: pointer;
`

const SearchInput = styled.div`
  position: relative;
  display: flex;
  background: #000000;
  height: 40px;
    border-radius: 20px;
  align-items: center;
  padding-left: 20px;
  padding-right: 20px;
  text-overflow: clip;
`

const Input = styled.input`
  font-size: 14px;
  padding-left: 10px;
  padding-right: 10px;
  background: none;
  border: none;
  width: 100%;
  min-width: 30px;
  margin: 0;
  color: #EEEEEE;
  font-family: 'Lexend Deca', sans-serif;
  cursor: pointer;
`

const Wrapper = styled.div`
  background: #111;
  position: relative;
  overflow-y: auto;
  overflow-x: hidden;
  z-index: ${({ searching }) => searching ? 101 : 99};
  grid-area: search;
  
  @media (max-width: 568px) {
    animation: ${({ searching }) => searching ? 'JumpInFromLeft cubic-bezier(0.445, 0.05, 0.55, 0.95) both 600ms' : 'none'};
    margin-left: ${({ searching }) => searching ? '60px' : 0};
    grid-area: book-form
  }
`

export default withRouter(Search)

import React, { useState } from 'react'
import styled from 'styled-components'

import RentForm from './BookForm'
import Search from './Search'

const Rent = () => {
  const [searching, setSearching] = useState(false)

  const toggleSearch = () => {
    setSearching(!searching)
  }

  return (
    <Wrapper>
      <RentForm searching={searching} toggleSearch={toggleSearch} />
      <Search searching={searching} toggleSearch={toggleSearch} />
    </Wrapper>
  )
}

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 400px auto;
  grid-template-areas: "book-form search";
  width: 100%;

  @media (max-width: 568px) {
    grid-template-columns: auto;
    grid-template-areas: "book-form";
  }
`

export default Rent

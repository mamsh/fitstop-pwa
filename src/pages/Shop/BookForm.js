import React from 'react'
import styled from 'styled-components'
import { Formik } from 'formik'
import { withRouter } from 'react-router-dom'
import * as yup from 'yup'
import { useGlobalState } from '../../libs/state'
import { displayDate } from '../../libs'
import { useMutation, useQuery } from '@apollo/react-hooks'
import gql from 'graphql-tag'

import Button from '../../components/Button'
import SelectDate from './SelectDate'
import Tip from '../../components/Tip'
import Loader from '../../components/Loader'
import Icon from  '../../components/Icon'

const FETCH_PRICING = gql`
  query {
    pricings: enum_pricing(order_by: { duration: asc }) {
      duration
      price
      type
    }
  }
`

const INSERT_BOOKING = gql`
  mutation insertBooking($item: [booking_insert_input!]!) {
    insert_booking(objects: $item) {
      affected_rows
      returning {
        id
        item_id
        pricing
        ride_date
        status
        start_time
        end_time
      }
    }
  }
`

const FETCH_ITEM_BOOKINGS = gql`
  query fetchItemBookings($itemId: uuid, $rideDate: date) {
    itemBookings: booking(where: {item_id: {_eq: $itemId}, status: {_in: ["pending", "renting"]}, ride_date: {_eq: $rideDate}}) {
      id
      item {
        id
        name
        status
      }
      ride_date
      start_time
      end_time
      status
    }
  }
`

const Input = ({ value, placeholder, onClick }) => (
  <StyledInput onClick={onClick}>
    { value || placeholder }
  </StyledInput>
)

const processDate = (date) => {
  const inPresent = date.getFullYear() === new Date().getFullYear() 
    && date.getMonth() === new Date().getMonth()
  
  if (inPresent) {
    switch (date.getDate() - new Date().getDate()) {
      case 0:
        return 'Today'
      case 1:
        return 'Tomorrow'
      default:
        return displayDate(date)
    }
  }
  return displayDate(date)
}

const BookForm = ({ history, searching, toggleSearch }) => {
  const [{ bookForm, currentUser }, globalDispatch] = useGlobalState()
  const [createBooking] = useMutation(INSERT_BOOKING)
  const { data } = useQuery(FETCH_PRICING)
  const { selectingDate, ...formValues } = bookForm
  const pricings = data ? data.pricings : null
  const fetchItemBookings = useQuery(FETCH_ITEM_BOOKINGS, {
    variables: formValues.id
      ? { itemId: formValues.id, rideDate: formValues.rideDate }
      : {}
  })

  const updateRideTime = value => {
    globalDispatch({
      bookForm: {
        ...formValues,
        time: {
          ...formValues.time,
          ...value
        }
      }
    })
  }

  return(
    <Formik
      enableReinitialize
      initialValues={formValues}
      validationSchema={
        yup.object().shape({
          name: yup.string().required('Required'),
          rideDate: yup.date().min(new Date(new Date().setDate(new Date().getDate() - 1)), `Ha ha! Don't dwell on the past, my friend.`),
          pricing: yup.string().required('Required'),
        })
      }
      onSubmit={async (values, { setSubmitting, setStatus, resetForm, setFieldError }) => {
        if (!currentUser) history.push('sign-in')
        else {
          let isNoonTime = false
          let hourSchema = yup.number().min(8).max(11) // AM
          if (values.time.meridian === 'PM') {
            if (parseInt(values.time.hour) === 12) {
              isNoonTime = true
            } else {
              hourSchema = yup.number().min(1).max(4)
            }
          }

          const isTimeValid = isNoonTime || await hourSchema.isValid(values.time.hour)

          if (!isTimeValid) {
            setFieldError('time', 'Oops, the store is not open at that time! Please book between 8:00AM to 4:00PM.')
            setSubmitting(false)
          } else {
            const { itemBookings } = fetchItemBookings.data
            const twoHoursInMs = 7200000
            const { id, rideDate, time, pricing } = values
            const startTimeString = `${time.hour}:${time.min} ${time.meridian}`
            const startTime = new Date(`${rideDate.toLocaleDateString()} ${startTimeString}`)
            const endTime = new Date(startTime)
            const duration = pricings.find(e => e.type === pricing).duration
            endTime.setTime(endTime.getTime() + duration)

            /**
             * Note:
             * bookings
             * 1: 11:20 - 12:00
             * 2. 14:50 - 15:10
             * 
             * chosen ride start and end time: 13:20 - 13:40
             * bookingsBefore: [{ 11:20 - 12:20 }]
             * bookingsAfter: [{ 14:50 - 15:10 }]
             */

            // bookings with ride start time before the current ride end time  
            let bookingsBefore = []
            // bookings with ride start time after the current ride end time
            let bookingsAfter = []

            itemBookings.forEach(booking => {
              const booked = new Date(`${booking.ride_date.toLocaleString()} ${booking.start_time}`)
              if (endTime.getTime() > booked.getTime()) bookingsBefore.push(booking)
              else bookingsAfter.push(booking)
            })

            const hasNoConflictWithBookingsBefore = bookingsBefore.every(booking => {
              const bookedStartTime = new Date(`${booking.ride_date.toLocaleString()} ${booking.start_time}`)
              return startTime.getTime() >= bookedStartTime.getTime() + twoHoursInMs
            })

            const hasNoConflictWithBookingsAfter = bookingsAfter.every(booking => {
              const bookedStartTime = new Date(`${booking.ride_date.toLocaleString()} ${booking.start_time}`)
              return endTime.getTime() <= bookedStartTime.getTime()
            })

            if (hasNoConflictWithBookingsBefore && hasNoConflictWithBookingsAfter) {
              createBooking({
                variables: {
                  item: [{
                    item_id: id,
                    ride_date: rideDate,
                    start_time: startTimeString,
                    end_time: `${endTime.getHours()}:${endTime.getMinutes()}`,
                    pricing,
                    user_id: currentUser.id
                  }]
                }
              })
              .then(() => {
                setSubmitting(false)
                globalDispatch({
                  bookForm: {
                    id: '',
                    name: '',
                    rideDate: new Date(),
                    time: {
                      hour: 8,
                      min: 0,
                      meridian: 'AM'
                    },
                    pricing: '1 hour',
                    rideDateSelectIsOpen: false,
                    ridePricingSelectIsOpen: false
                  },
                })
                setStatus({type: 'success', message: 'Booked!'})
              })
              .catch((error) => {
                setSubmitting(false)
                setStatus({type: 'error', message: error.message})
              })
            } else {
              setFieldError('name', `${values.name} is not available.`)
              setSubmitting(false)
            }
          }
        }
      }}
    >
      {({ values, errors, touched, setFieldValue, handleSubmit, isSubmitting, status }) => {
        return (
          <>
            {isSubmitting && <Loader />}
            <Wrapper>
              {searching && 
                <Close onClick={() => {
                    if (searching) {
                      toggleSearch()
                    }
                  }}
                >
                <Icon name='clear' />
              </Close>}
              <FormWrapper>
                {status && <Tip type={status.type} message={status.message} arrow='none' />}
                <FormItem>
                  <Label>I want to ride</Label>
                  <Input onClick={toggleSearch} value={values.name} placeholder='Please select a bike' />
                  {errors.name && touched.name && <Tip type='error' message={errors.name} />}
                </FormItem>
                <FormItem>
                  {(processDate(values.rideDate) !== 'Today' && processDate(values.rideDate) !== 'Tomorrow') && <Label>on</Label>}
                  <Input
                    value={processDate(values.rideDate)} 
                    onClick={() => {
                      globalDispatch({
                        bookForm: {
                          ...formValues,
                          selectingDate: !selectingDate
                        }
                      })
                    }}
                  />
                  {selectingDate && <SelectDate
                    selectedValue={processDate(values.rideDate)}
                    onChange={(value) => {
                      globalDispatch({
                        bookForm: {
                          ...formValues,
                          selectingDate,
                          rideDate: value
                        }
                      })
                      setFieldValue('rideDate', value)
                    }}
                  />}
                  {errors.rideDate && <Tip type='error' message={errors.rideDate} />}
                </FormItem>
                <FormItem>
                  <Label>at</Label>
                  <TimeWrapper>
                    <Select value={formValues.time.hour} onChange={e => updateRideTime({ hour: e.target.value })}>
                      {
                        [...Array(12).keys()].map((hour) => {
                          const value = hour + 1
                          const paddedValue = (value + '').padStart(2, '0')
                          return (
                            <option key={hour} value={value}>{paddedValue}</option>
                          )
                        })
                      }
                    </Select>:
                    <Select value={formValues.time.min} onChange={e => updateRideTime({ min: e.target.value })}>
                      {
                        [...Array(12).keys()].map((minute) => {
                          const value = minute * 5
                          const paddedValue = (value + '').padStart(2, '0')
                          return (
                            <option key={minute} value={value}>{paddedValue}</option>
                          )
                        })
                      }
                    </Select>
                    <Select value={formValues.time.meridian} onChange={e => updateRideTime({ meridian: e.target.value })}>
                      <option>AM</option>
                      <option>PM</option>
                    </Select>
                  </TimeWrapper>
                  {errors.time && touched.time && <Tip type='error' message={errors.time} />}
                </FormItem>
                <FormItem>
                  <Label>for</Label>
                  <PricingSelect value={values.pricing} onChange={(e) => {
                    const value = e.target.value
                    globalDispatch({
                      bookForm: {
                        ...formValues,
                        pricing: value
                      }
                    })
                    setFieldValue('pricing', value)
                  }}>
                    {
                      pricings && pricings.map(({type}) => (
                        <option key={type} value={type}>{type}</option>
                      ))
                    }
                  </PricingSelect>
                </FormItem>
                <FormItem>
                  <Label>You will pay</Label>
                  <Input value={`₱ ${pricings ? pricings.find(pricing => pricing.type === values.pricing).price : 0}.00 `} />
                </FormItem>
                <FormButtom onClick={!isSubmitting && handleSubmit}>
                  <Button text="Let's book it!" type='primary' disabled={isSubmitting} />
                </FormButtom>
              </FormWrapper>
            </Wrapper>
          </>
        )
      }
    }
    </Formik>
  )
}

const Close = styled.div`
  position: absolute;
  z-index: 200;
  cursor: pointer;
  color: #EEEEEE;
  top: 0;
  bottom: 0;
  left: 0;
  height: 100%;
  width: 60px;
  background: black;
  align-items: center;
  justify-content: center;
  font-size: 18px;
  display: none;
  opacity: 0.8;
  animation: JumpInFromRight cubic-bezier(0.445, 0.05, 0.55, 0.95) both 600ms;

  @media (max-width: 568px) {
    display: flex;
  }
`

const Select = styled.select`
  all: unset;
  border-bottom: solid 2px;
  color: #EEEEEE;
  font-size: 20px;
  width: auto;
  margin: 0 6px;
  cursor: pointer;
  :first-child {
    margin-left: 0;
  }
  option {
    font-size: initial;
    color: initial;
  }
`

const PricingSelect = styled(Select)`
  margin: 0;
  color: #EEEEEE !important;
`

const TimeWrapper = styled.div`
  display: flex;
  color: #EEEEEE;
  font-size: 20px;
`

const FormWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-items: center;
  justify-content: center;
  padding: 40px;
  padding-left: 80px;
  padding-right: 80px;
  position: relative;

  @media (max-width: 400px) {
    padding-left: 40px;
    padding-right: 40px;
  }
`

const Wrapper = styled.div`
  grid-area: book-form;
  background: #191919;
  overflow: auto;
  display: grid;
  position: relative;
  z-index: 100;
`

const Label = styled.div`
  font-size: 14px;
  color: #EEEEEE;
  opacity: 0.5;
`

const StyledInput = styled.span`
  font-size: 20px;
  color: #EEEEEE;
  font-family: 'Lexend Deca', sans-serif;
  white-space: nowrap;
  border-bottom: solid 2px;
  position: relative;
  display: flex;
  cursor: pointer;
`

const FormItem = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  :not(:first-child) {
    margin-top: 40px;
  }
`

const FormButtom = styled.div`
  margin-top: 40px !important;
`

export default withRouter(BookForm)

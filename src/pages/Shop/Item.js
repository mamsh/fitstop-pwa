import React from 'react'
import styled from  'styled-components'
import { withRouter } from 'react-router-dom'
import { useMutation, useSubscription } from '@apollo/react-hooks'
import gql from 'graphql-tag'
import { useGlobalState } from  '../../libs/state'
import { isAdmin } from '../../libs'

import Icon from '../../components/Icon'

const CHANGE_ITEM_STATUS = gql`
  mutation changeItemStatus($filter: item_bool_exp!, $item: item_set_input) {
    update_item(where: $filter, _set: $item) {
      affected_rows
      returning {
        id
        name
        status
      }
    }
  }
`

const FETCH_ITEM = gql`
  subscription fetchItem($filter: item_bool_exp!) {
    item(where: $filter) {
      id
      name
      status
      pic
    }
  }  
`

const Item = ({ index, id, history, toggleSearch }) => {
  const [changeItemStatus, changeItemStatusResult] = useMutation(CHANGE_ITEM_STATUS)
  const { data, loading } = useSubscription(FETCH_ITEM, {
    variables: {
      filter: {
        id: {
          _eq: id
        }
      }
    }
  })
  const [{ currentUser, bookForm }, globalDispatch] = useGlobalState()
  const rentItem = item => {
    globalDispatch({
      bookForm: { ...bookForm, ...item }
    })
  }
  const { status, name, pic } = data ? data.item[0] : {}
  
  return (
    <Wrapper 
      index={index}
      style={{ backgroundImage: `url(${pic})`, backgroundSize: 'cover', backgroundPosition: 'center' }}
      loading={+loading || +changeItemStatusResult.loading} // force boolean to be 1 or 0
    >
      <Info>
        <span>{name}</span>
        <Status status={status}>{status}</Status>
      </Info>
      {!pic && <IconWrapper><Icon name='directions_bike' /></IconWrapper>}
      {!loading && <Actions>
      {
        isAdmin(currentUser) &&
          <>
            {status === 'deleted'
            ? <Action onClick={() => changeItemStatus({
              variables: {
                  filter: {
                    id: { _eq: id }
                  },
                  item: {
                    status: 'available',
                  }
                }
              })}>Restore</Action>
              : <Action onClick={() => changeItemStatus({
                  variables: {
                    filter: {
                      id: { _eq: id }
                    },
                    item: {
                      status: 'deleted',
                    }
                  }
                }
              )}>Delete</Action>
            }
            <Action onClick={() => history.push('edit-item', { id })}>Edit</Action>
          </>
        }
        {
          bookForm.id === id ?
          <Action onClick={() => {
            rentItem({ id: '', name: '' })
            toggleSearch()
          }}>Unbook</Action> : 
          <Action onClick={() => {
            rentItem({ id, name })
            toggleSearch()
          }}>Book</Action>
        }
      </Actions>}
    </Wrapper>
  )
}

const STATUS_COLORS = {
  'available': '#43A047',
  'repair': '#FB8C00',
  'deleted': '#E53935'
}

const IconWrapper = styled.div`
  position: absolute;
  height: 100%;
  width: 100%;
  display: flex;
  i {
    margin: auto;
    font-size: 28px;
  }
`

const Action = styled.div`
  color: #FBE9E7;
  display: flex;
  font-size: 12px;
  opacity: 1;
  align-items: center;
  transition: 500ms;
  white-space: nowrap;
  cursor: pointer;
  i {
    font-size: 18px;
    margin-right: 4px;
  }
  :not(:last-child) {
    margin-right: 10px;
  }
`

const Info = styled.div`
  position: absolute;
  display: flex;
  align-items: flex-end;
  padding: 20px;
  top: 20%;
  bottom: 0;
  right: 0;
  left: 0;
  border-bottom-left-radius: 10px;
  border-bottom-right-radius: 10px;
  background: linear-gradient(transparent, black);
  overflow: auto;
  white-space: nowrap;
`

const Actions = styled.div`
  position: absolute;
  justify-content: flex-end;
  align-items: flex-start;
  display: flex;
  padding: 20px;
  bottom: 80%;
  top: 0;
  right: 0;
  left: 0;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  background: linear-gradient(black, transparent);
`

const Status = styled.span`
  color: #FBE9E7;
  border-radius: 16px;
  font-size: 10px;
  margin-left: 10px;
  text-transform: uppercase;
  padding: 10px;
  padding-top: 3px;
  padding-bottom: 4px;
  margin-bottom: -1px;
  background: ${({status}) => STATUS_COLORS[status]};
` 

const Wrapper = styled.div`
  border-radius: 10px;
  color: #EEEEEE;
  width: 260px;
  padding-bottom: 260px;
  height: 0;
  margin: 10px;
  float: left;
  background: #212121;
  position: relative;
  animation: Bounce cubic-bezier(0.445, 0.05, 0.55, 0.95) both 500ms;
  animation-delay: ${({index}) => index * 100}ms;
  ${({loading}) => loading && 'opacity: 0.5 !important' };
  :last-child {
    margin-bottom: 30px;
  }

  @media (max-width: 1024px) {
    width: calc(50% - 20px);
    padding-bottom: calc(50% - 20px);
  }

  @media (max-width: 868px) {
    width: calc(100% - 20px);
    padding-bottom: calc(100% - 20px);
  }
`

export default withRouter(Item)

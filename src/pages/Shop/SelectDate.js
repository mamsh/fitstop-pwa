import React, { useState } from 'react'
import styled from 'styled-components'
import { useGlobalState } from  '../../libs/state'

import Icon from '../../components/Icon'

const SelectDate = ({ onChange, selectedValue }) => {
  const [{ bookForm }, globalDispatch] = useGlobalState()
  const dateToday = new Date()
  const selectedMonth = Object.keys(dates)[bookForm.rideDate.getMonth()]
  let defaultDate = bookForm.rideDate.getDate()
  const inPresent = bookForm.rideDate.getFullYear() === new Date().getFullYear() &&
                    bookForm.rideDate.getMonth() === new Date().getMonth()

  if (inPresent) {
    // used to represent day(s) after 'tomorrow'
    defaultDate = bookForm.rideDate.getDate() - new Date().getDate() > 2
    ? bookForm.rideDate.getDate()
    : new Date().getDate() + 2
  }

  const [customDate, setCustomDate] = useState(defaultDate) // used to override default 'date' value of 'rideDate'

  const close = () => {
    globalDispatch({
      bookForm: {
        ...bookForm,
        selectingDate: false
      }
    })
  }

  return (
    <Wrapper>
      <CloseWrapper onClick={close}><Icon name='clear' /></CloseWrapper>
      <Title>When is the ride?</Title>
      <Option 
        selected={selectedValue === 'Today'} 
        onClick={() => onChange(dateToday)}
      >
        Today
      </Option>
      <Option 
        selected={selectedValue === 'Tomorrow'} 
        onClick={() => onChange(new Date(dateToday.setDate(dateToday.getDate() + 1)))}
      >
        Tomorrow
      </Option>
      <Option selected={!isNaN(new Date(selectedValue))}>
        <SelectWrapper>
          <Select
            defaultValue={bookForm.rideDate.getMonth()}
            onClick={e => {
              const newFullDate = new Date(new Date(bookForm.rideDate))
              newFullDate.setMonth(e.target.value)
              newFullDate.setDate(customDate)
              onChange(newFullDate)
            }
          }>
            {
              Object.keys(dates).map((month, index) => (
                <option key={index} value={index}>{month}</option>
              ))
            }
          </Select>
          <Select
            defaultValue={defaultDate}
            onClick={e => {
              onChange(new Date(new Date(bookForm.rideDate).setDate(e.target.value)))
              setCustomDate(e.target.value)
            }
          }>
            {
              [...Array(dates[selectedMonth]).keys()].map((day) => (
                <option key={day}>{day + 1}</option>
              ))
            }
          </Select>
          <Select
            defaultValue={bookForm.rideDate.getFullYear()}
            onClick={e => {
              const newFullDate = new Date(new Date(bookForm.rideDate))
              newFullDate.setFullYear(e.target.value)
              newFullDate.setDate(customDate)
              onChange(newFullDate)
            }
          }>
            {
              [new Date().getFullYear(), new Date().getFullYear() + 1].map((year, index) => (
                <option key={index}>{year}</option>
              ))
            }
          </Select>
        </SelectWrapper>
      </Option>
    </Wrapper>
  )
}

const dates = {
  'January': 31,
  'February': 29,
  'March': 31,
  'April': 30,
  'May': 31,
  'June': 30,
  'July': 31,
  'August': 31,
  'September': 30,
  'October': 31,
  'November': 30,
  'December': 31,
}

const SelectWrapper = styled.div`
  display: flex;
`

const Select = styled.select`
  all: unset;
  border-bottom: solid 2px;
  margin: 0 6px;
  cursor: pointer;
  :first-child {
    margin-left: 0;
  }
  option {
    font-size: initial;
    color: initial;
  }
`

const Option = styled.div`
  margin: 10px 0;
  opacity: ${({ selected }) => selected ? 1 : 0.5};
  font-size: 20px;
  position: relative;
  color: #EEEEEE;
  cursor: pointer;
  :hover {
    opacity: 1;
  }
`

const Title = styled.div`
  font-size: 14px;
  color: #EEEEEE;
  opacity: 0.5;
`

const CloseWrapper = styled.div`
  position: absolute;
  font-size: 14px;
  color: #EEEEEE;
  opacity: 0.5;
  font-size: 18px;
  top: 10px;
  right: 10px;
  cursor: pointer;
`

const Wrapper = styled.div`
  background: #000000;
  padding: 40px 80px;
  position: absolute;
  z-index: 1000;
  top: 50%;
  left: 0;
  right: 0;
  bottom: 0;
  animation: JumpIn cubic-bezier(0.445, 0.05, 0.55, 0.95) both 600ms;

  @media (max-width: 400px) {
    padding-left: 40px;
    padding-right: 40px;
  }
`

export default SelectDate

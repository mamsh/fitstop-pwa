import React, { useEffect } from 'react'
import styled from 'styled-components'
import { Formik } from 'formik'
import * as yup from 'yup'
import { withRouter } from 'react-router-dom'
import withFirebase from '../hocs/withFirebase'
import { useGlobalState, initialState } from '../libs/state'

import Button from '../components/Button'
import Tip from '../components/Tip'
import Loader from '../components/Loader'

const SignIn = ({ history, firebase }) => {
  const [{ currentUser }, globalDispatch] = useGlobalState()
  // sign out user when sign in page is mounted
  useEffect(() => {
    if (currentUser) {
      firebase.doSignOut()
      globalDispatch(initialState)
    }
  }, [currentUser, firebase, globalDispatch])

  return (
    <Formik
      initialValues={{
        email: '',
        password: ''
      }}
      validationSchema={
        yup.object().shape({
          email: yup
            .string()
            .email('Invalid email')
            .required('Required'),
          password: yup
            .string()
            .required('Required')
            .min(8, 'Password must be at least 8 characters'),
        })
      }
      onSubmit={(values, { setSubmitting, setStatus }) => {
        setSubmitting(true)
        firebase
          .doSignInWithEmailAndPassword(values.email, values.password)
          .then(() => {
            history.push('shop')
          })
          .catch((error) => {
            setSubmitting(false)
            if (error && error.code === 'auth/user-not-found') {
              setStatus({ type: 'error', message: 'Email and password combination not found' })
            } else {
              setStatus({ type: 'error', message: error.message })
            }
          })
      }}
    >
      {({ values, handleChange, errors, touched, handleSubmit, isSubmitting, status }) => {
        return (
          <>
            {isSubmitting && <Loader />}
            <Wrapper onKeyPress={(e) => e.key === 'Enter' && !isSubmitting && handleSubmit()}>
              {status && <Tip type={status.type} message={status.message} arrow='none' />}              
              <FormItem>
                <Label for='email'>Email</Label>
                <Input 
                  id='email'
                  name='email'
                  type='email'
                  onChange={handleChange}
                  value={values.email} 
                />
              </FormItem>
              {touched.email && errors.email && <Tip type='error' message={errors.email} />}
              <FormItem>
                <Label for='password'>Password</Label>
                <Input 
                  id='password'
                  name='password'
                  type='password'
                  onChange={handleChange}
                  value={values.password} 
                />
              </FormItem>
              {touched.password && errors.password && <Tip type='error' message={errors.password} />}
              <FormButtom onClick={!isSubmitting && handleSubmit}>
                <Button text='Sign in' type='primary' disabled={isSubmitting} />
              </FormButtom>
              <FormButtom onClick={() => history.push('sign-up')}>
                <Button text='No account? Sign up now!' />
              </FormButtom>
            </Wrapper>
          </>
        )
      }
    }
    </Formik>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  width: 400px;
  position: relative;
  padding: 40px;

  @media (max-width: 400px) {
    width: calc(100% - 80px);
  }
`

const Label = styled.label`
  font-size: 14px;
  color: #EEEEEE;
  opacity: 0.5;
  white-space: nowrap;
  text-overflow: clip;
  cursor: pointer;
`

const Input = styled.input`
  font-size: 14px;
  padding-left: 10px;
  background: none;
  border: none;
  margin: 0;
  color: #EEEEEE;
  flex: 1;
  font-family: 'Lexend Deca', sans-serif;
  cursor: pointer;
`

const FormItem = styled.div`
  position: relative;
  display: flex;
  overflow: hidden;
  background: #000000;
  height: 40px;
  border-radius: 20px;
  align-items: center;
  padding-left: 20px;
  padding-right: 20px;
  text-overflow: clip;
  :not(:first-child) {
    margin-top: 20px;
  }
`

const FormButtom = styled.div`
  margin-top: 20px !important;
`

export default withRouter(withFirebase(SignIn))

import React, { useState } from 'react'
import styled from 'styled-components'
import gql from 'graphql-tag'
import { useSubscription } from '@apollo/react-hooks'

import Booking from  './Booking'

const FETCH_BOOKINGS = gql`
  subscription {
    result: booking(order_by: { created_at: desc }) {
      id
      status
    }
  }
`

const TabSelector = ({ active, setActive }) => (
  <TabSelectorWrapper>
    <Tab active={active === 'pending'}onClick={() => setActive('pending')}>Pending</Tab>
    <Tab active={active === 'renting'}onClick={() => setActive('renting')}>Renting</Tab>
    <Tab active={active === 'returned'}onClick={() => setActive('returned')}>Returned</Tab>
    <Tab active={active === 'cancelled'}onClick={() => setActive('cancelled')}>Cancelled</Tab>
  </TabSelectorWrapper>
)

const Column = ({ title, list, active, setActive }) => (
  <ColumnWrapper active={active === title}>
    <Title>{title}</Title>
    <TabSelector active={active} setActive={setActive} />
    <List>{
      list.length 
      ? list.map((booking, index) => <Booking key={index} index={index} id={booking.id} />) 
      : <EmptyWrapper>No bookings yet</EmptyWrapper>
    }</List>
  </ColumnWrapper>
)

const Bookings = () => {
  const [active, setActive] = useState('pending')
  const { data } = useSubscription(FETCH_BOOKINGS)
  const bookings = data ? data.result : []

  return (
    <Wrapper>
      <Column active={active} setActive={setActive} title='pending' list={bookings.filter((booking) => booking.status === 'pending')} />
      <Column active={active} setActive={setActive} title='renting' list={bookings.filter((booking) => booking.status === 'renting')} />
      <Column active={active} setActive={setActive} title='returned' list={bookings.filter((booking) => booking.status === 'returned')} />
      <Column active={active} setActive={setActive} title='cancelled' list={bookings.filter((booking) => booking.status === 'cancelled')} />
    </Wrapper>
  )
}

const EmptyWrapper = styled.div`
  margin-top: 20px;
  opacity: 0.5;
`  

const Title = styled.div`
  font-size: 16px;
  color: #EEEEEE;
  border-bottom: solid 2px;
  padding-bottom: 4px;
  text-transform: capitalize;

  @media (max-width: 964px) {
    display: none;
  }
`

const TabSelectorWrapper = styled.div`
  display: none;
  @media (max-width: 964px) {
    width: calc(100% - 80px);
    display: flex;
  }
`

const Tab = styled.div`
  font-size: 16px;
  color: #EEEEEE;
  padding-bottom: 4px;
  text-transform: capitalize;
  max-width: 25%;
  text-overflow: clip;
  cursor: pointer;
  :hover {
    opacity: 1;
  }
  overflow: ${({active}) => active ? 'visible' : 'hidden'};
  opacity: ${({active}) => active ? 1 : 0.5};
  border-bottom: ${({active}) => active ? 'solid 2px' : 'none'};
  :not(:last-child) {
    margin-right: 20px;
  }
`

const List = styled.div`
  overflow-y: scroll;
  overflow-x: hidden;
  width: 100%;
  @media (max-width: 964px) {
    width: calc(100% - 80px);
  }
`

const ColumnWrapper = styled.div`
  color: #EEEEEE;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  height: 100%;
  width: 25%;
  :not(:last-child) {
    margin-right: 20px;
  }
  @media (max-width: 964px) {
    display: ${({active}) => active ? 'flex' : 'none'};
    width: 400px;
    max-width: 400px;
    margin: 0 !important;
    align-items: center;
  }
`

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  padding: 40px;
  padding-bottom: 0;
  background: #111111;
  overflow: hidden;
`

export default Bookings

import React from 'react'
import styled from 'styled-components'
import gql from 'graphql-tag'
import { displayDate, displayTime } from '../../libs'
import { useSubscription, useMutation } from '@apollo/react-hooks'

import Icon from '../../components/Icon'

const FETCH_BOOKING = gql`
  subscription fetchBooking($filter: booking_bool_exp!) {
    booking(where: $filter) {
      id
      user {
        first_name
        last_name
      }
      item {
        name
      }
      status
      created_at
      ride_date
      start_time
      enum_pricing {
        price
        type
      }
    }
  }  
`

const CHANGE_BOOKING_STATUS = gql`
  mutation changeBookingStatus($filter: booking_bool_exp!, $booking: booking_set_input) {
    update_booking(where: $filter, _set: $booking) {
      affected_rows
      returning {
        id
      }
    }
  }
`

const Booking = ({ index, id }) => {
  const { data, loading } = useSubscription(FETCH_BOOKING, {
    variables: {
      filter: {
        id: {
          _eq: id
        }
      }
    }
  })
  const [changeBookingStatus,] = useMutation(CHANGE_BOOKING_STATUS)
  const { user, item, created_at, ride_date, start_time, status, enum_pricing } = data ? data.booking[0] : {}
  const isNew = created_at && (new Date - new Date(created_at)) < 3600000 // less than 1 hour
  
  return (
    <Wrapper isNew={isNew} index={index}>
      <Status status={status}>
        {status}
        <Select onChange={(e) => changeBookingStatus({
          variables: {
              filter: {
                id: { _eq: id }
              },
              booking: {
                status: e.target.value,
              }
            }
          })
        }>
          <option value='pending'>Pending</option>
          <option value='renting'>Renting</option>
          <option value='returned'>Returned</option>
          <option value='cancelled'>Cancelled</option>
        </Select>  
      </Status>
      <NewIconWrapper>
        {isNew && <Icon name='new_releases' />}
      </NewIconWrapper>
      {
        !loading && data.booking.length &&
        <>
          <Info style={{ fontSize: '14px', marginBottom: '4px' }}>{`${user && user.first_name} ${user && user.last_name}`}</Info>
          <Info><Icon name='directions_bike' />{item.name}</Info>
          <Info>
            <div><Icon name='calendar_today' />{displayDate(new Date(ride_date), true)}</div>
            <div><Icon name='access_time' />{displayTime(start_time)}</div>
          </Info>
          <Info>
            <div><Icon name='timelapse' />{enum_pricing.type}</div>
            <div><Icon name='local_offer' />₱{enum_pricing.price}.00</div>
          </Info>
        </> 
      }
    </Wrapper>
  )
}

const Info = styled.div`
  display: flex;
  align-content: center;
  font-size: 12px;
  margin-top: 2px;
  i {
    margin-right: 6px;
    opacity: 0.5;
    margin-top: 1px;
  }
  div {
    display: flex;
    align-content: center;
    white-space: nowrap;
    overflow:  hidden;
    text-overflow: ellipsis;
    :first-child {
      margin-right: 6px;
      margin-bottom: 1px;
    }
  }
`

const STATUS_COLORS = {
  'renting': '#43A047',
  'pending': '#FB8C00',
  'returned': '#3949AB',
  'cancelled': '#E53935'
}

const Select = styled.select`
  all: unset;
  opacity: 0;
  position: absolute;
  right: 0;
  left: 0;
  option {
    font: initial;
    background: initial;
  }
`

const Status = styled.div`
  color: #FBE9E7;
  position: absolute;
  top: 10px;
  right: 10px;
  border-radius: 16px;
  font-size: 10px;
  margin-left: 10px;
  text-transform: uppercase;
  text-align: center;
  padding: 10px;
  align-self: flex-end;
  padding-top: 3px;
  padding-bottom: 4px;
  margin-bottom: -1px;
  background: ${({status}) => STATUS_COLORS[status]};
`

const NewIconWrapper = styled.div`
  position: absolute;
  top: 4px;
  left: 6px;
  font-size: 18px;
  color: #3949AB;
`

const Wrapper = styled.div`
  background: #191919;
  position: relative;
  color: #EEEEEE;
  border-radius: 10px;
  padding: 10px 20px;
  margin-top: 20px;
  min-height: 100px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: calc(100% - 40px);
  :last-child {
    margin-bottom: 40px;
  }
  ${({ isNew }) => isNew && `
    background: #212121;
  `};
  animation: Bounce cubic-bezier(0.445, 0.05, 0.55, 0.95) both 500ms;
  animation-delay: ${({index}) => index * 100}ms;
`

export default Booking
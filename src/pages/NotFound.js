import React from 'react'
import styled from 'styled-components'
import { withRouter } from 'react-router-dom'

import Button from '../components/Button'

const NotFound = ({ history }) => (
  <Wrapper>
    <Button
      text='Page not found!'
      type='landing'
      onClick={() => history.push('/')}
    />
  </Wrapper>
)

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  width: 400px;
  position: relative;
  opacity: ${({ isSubmitting }) => isSubmitting ? 0.5 : 1};
  padding: 40px;
`

export default withRouter(NotFound)

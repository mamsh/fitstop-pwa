import React from 'react'
import styled from 'styled-components'
import { Formik } from 'formik'
import * as yup from 'yup'
import { withRouter } from 'react-router-dom'
import gql from 'graphql-tag'
import { useMutation, useQuery } from '@apollo/react-hooks'

import Button from '../components/Button'
import Tip from '../components/Tip'
import Loader from '../components/Loader'
import SelectFormItem from '../components/SelectFormItem'
import PicFormItem from '../components/PicFormItem'

const CREATE_ITEM = gql`
  mutation createItem($item: [item_insert_input!]!) {
    insert_item(objects: $item) {
      affected_rows
      returning {
        id
      }
    }
  }
`

const UPDATE_ITEM = gql`
  mutation updateItem($filter: item_bool_exp!, $item: item_set_input) {
    update_item(where: $filter, _set: $item) {
      returning {
        id
        name
        status
      }
    }
  }
`

const FETCH_ITEM_LOCALLY = gql`
  query fetchItemLocally($filter: item_bool_exp) @client {
    results: item(where: $filter) {
      id
      name
      status
      pic
    }
  }
`

const ItemForm = ({ history }) => {
  const { state } = history.location
  const isEditing = state && state.id
  const [createItem] = useMutation(CREATE_ITEM)
  const [updateItem] = useMutation(UPDATE_ITEM)
  const { data, loading } = useQuery(FETCH_ITEM_LOCALLY, {
    variables: {
      filter: isEditing ? {
        id: { _eq: state.id }
      } : {}
    }
  })

  const initialValues = isEditing && data.results && data.results.length
  ? {
    ...data.results[0]
  } : {
    id: '',
    name: '',
    status: 'available',
    pic: ''
  }

  return (
    <Formik
      enableReinitialize
      initialValues={initialValues}
      validationSchema={
        yup.object().shape({
          name: yup
            .string()
            .required('Required'),
          status: yup
            .string()
        })
      }
      onSubmit={(values, { setSubmitting, setStatus, resetForm }) => {
        setSubmitting(true)
        const formHandler = isEditing ? updateItem({
          variables: {
            filter: {
              id: { _eq: state.id }
            },
            item: {
              name: values.name,
              status: values.status,
              pic: values.pic
            }
          }
        }) : createItem({
          variables: {
            item: [{
              name: values.name,
              status: values.status,
              pic: values.pic
            }]
          }
        })

        formHandler
        .then(() => {
          setSubmitting(false)
          if (!isEditing) {
            resetForm({ name: '', status: 'available', pic: '' })
          }
          setStatus({type: 'success', message: `Item ${isEditing ? 'edited' : 'added'}!`})
        })
        .catch((error) => {
          setSubmitting(false)
          setStatus({type: 'error', message: error.message})
        })
      }}
    >
      {({ values, handleChange, errors, touched, handleSubmit, setFieldValue, setFieldError, isSubmitting, status }) => {
        return (
          <>
            {isSubmitting && <Loader />}
            <Wrapper onKeyPress={(e) => e.key === 'Enter' && !isSubmitting && handleSubmit()}>
              {status && <Tip type={status.type} message={status.message} arrow='none' />}              
              <FormItem>
                <Label for='name'>Item name</Label>
                <Input 
                  id='name'
                  name='name'
                  onChange={handleChange}
                  value={values.name} 
                />
              </FormItem>
              {touched.name && errors.name && <Tip type='error' message={errors.name} />}
              <SelectFormItem
                label='Status'
                name='status'
                placeholder='Select status'
                value={values.status}
                onChange={(value) => setFieldValue('status', value)}
                options={['available', 'repair', 'deleted']}
              />               
              <PicFormItem 
                name='pic'
                onChange={(value) => setFieldValue('pic', value)}
                onError={(message) => setFieldError('pic', message)}
                value={values.pic} 
              />
              {errors.pic && <Tip type='error' message={errors.pic} />}
              <FormButtom onClick={handleSubmit}>
                <Button text={isEditing ? 'Edit item' : 'Add item'} type='primary' disabled={isSubmitting || loading} disabledText='Loading' />
              </FormButtom>
            </Wrapper>
          </>
        )
      }
    }
    </Formik>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  width: 400px;
  position: relative;
  padding: 40px;
  justify-content: center;

  @media (max-width: 400px) {
    width: calc(100% - 80px);
  }
`

const Label = styled.label`
  font-size: 14px;
  white-space: nowrap;
  color: #EEEEEE;
  opacity: 0.5;
  cursor: pointer;
`

const Input = styled.input`
  font-size: 14px;
  padding-left: 10px;
  background: none;
  border: none;
  margin: 0;
  color: #EEEEEE;
  width: 100%;
  font-family: 'Lexend Deca', sans-serif;
  white-space: nowrap;
  cursor: pointer;
`

const FormItem = styled.div`
  display: flex;
  background: #000000;
  height: 40px;
  border-radius: 20px;
  align-items: center;
  padding-left: 20px;
  padding-right: 20px;
  :not(:first-child) {
    margin-top: 20px;
  }
`

const FormButtom = styled.div`
  margin-top: 20px !important;
`

export default withRouter(ItemForm)

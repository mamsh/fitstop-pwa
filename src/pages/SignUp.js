import React from 'react'
import styled from 'styled-components'
import { Formik } from 'formik'
import * as yup from 'yup'
import { withRouter } from 'react-router-dom'
import withFirebase from '../hocs/withFirebase'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

import Button from '../components/Button'
import Tip from '../components/Tip'
import Loader from '../components/Loader'

const CREATE_USER = gql`
  mutation createUser($user: [user_insert_input!]!) {
    insert_user(objects: $user) {
      affected_rows
      returning {
        id
        first_name
        last_name
        email
      }
    }
  }
`

const validationSchema = yup.object().shape({
  firstName: yup
    .string()
    .required('Required'),
  lastName: yup
    .string()
    .required('Required'),
  email: yup
    .string()
    .email('Invalid email')
    .required('Required'),
  password: yup
    .string()
    .required('Required')
    .min(8, 'Password must be at least 8 characters'),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref('password'), null], 'Passwords must match')
    .required('Required'),
})

const initialValues = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  confirmPassword: '',
}

const SignUp = ({ history, firebase }) => {
  const [createUser] = useMutation(CREATE_USER)

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={(values, { setSubmitting, setStatus, resetForm }) => {
        let firebaseUser = null
        setSubmitting(true)
        firebase
          .doCreateUserWithEmailAndPassword(values.email, values.password)
          .then((result) => {
            firebaseUser = result.user
            return createUser({
              variables: {
                user: [{
                  email: values.email,
                  first_name: values.firstName,
                  last_name: values.lastName,
                  uid: firebaseUser.uid
                }]
              }
            })
          })
          .then(() => {
            setSubmitting(false)
            resetForm()
            setStatus({type: 'success', message: 'Cheers! You may sign in now!'})
          })
          .catch((error) => {
            setSubmitting(false)
            setStatus({type: 'error', message: error.message})
          })
      }}
    >
      {({ values, handleChange, touched, errors, handleSubmit, isSubmitting, status }) => {
        return (
          <>
            {isSubmitting && <Loader />}
            <Wrapper onKeyPress={(e) => e.key === 'Enter' && !isSubmitting && handleSubmit()}>
              {status && <Tip type={status.type} message={status.message} arrow='none' />}
              <FormItem>
                <Label for='firstName'>First Name</Label>
                <Input 
                  id='firstName'
                  name='firstName'
                  onChange={handleChange}
                  value={values.firstName}
                  />
              </FormItem>
              {touched.firstName && errors.firstName && <Tip type='error' message={errors.firstName} />}
              <FormItem>
                <Label for='lastName'>Last Name</Label>
                <Input 
                  id='lastName'
                  name='lastName'
                  onChange={handleChange}
                  value={values.lastName} 
                />
              </FormItem>
              {touched.lastName && errors.lastName && <Tip type='error' message={errors.lastName} />}
              <FormItem>
                <Label for='email'>Email</Label>
                <Input 
                  id='email'
                  name='email'
                  type='email'
                  onChange={handleChange}
                  value={values.email} 
                />
              </FormItem>
              {touched.email && errors.email && <Tip type='error' message={errors.email} />}
              <FormItem>
                <Label for='password'>Password</Label>
                <Input 
                  id='password'
                  name='password'
                  type='password'
                  onChange={handleChange}
                  value={values.password} 
                />
              </FormItem>
              {touched.password && errors.password && <Tip type='error' message={errors.password} />}
              <FormItem>
                <Label for='confirmPassword'>Confirm password</Label>
                <Input 
                  id='confirmPassword'
                  name='confirmPassword'
                  type='password'
                  onChange={handleChange}
                  value={values.confirmPassword} 
                />
              </FormItem>
              {touched.confirmPassword && errors.confirmPassword && <Tip type='error' message={errors.confirmPassword} />}
              <FormButtom onClick={!isSubmitting && handleSubmit}>
                <Button text='Sign up' type='primary' disabled={isSubmitting} />
              </FormButtom>
              <FormButtom onClick={() => history.push('sign-in')}>
                <Button text='Already have an account? Sign in!' />
              </FormButtom>
            </Wrapper>
          </>
        )
      }
    }
    </Formik>
  )
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: auto;
  width: 400px;
  position: relative;
  padding: 40px;

  @media (max-width: 400px) {
    width: calc(100% - 80px);
  }
`

const Label = styled.label`
  font-size: 14px;
  color: #EEEEEE;
  opacity: 0.5;
  white-space: nowrap;
  text-overflow: clip;
  cursor: pointer;
`

const Input = styled.input`
  font-size: 14px;
  padding-left: 10px;
  background: none;
  border: none;
  flex: 1;
  margin: 0;
  color: #EEEEEE;
  font-family: 'Lexend Deca', sans-serif;
  cursor: pointer;
`

const FormItem = styled.div`
  position: relative;
  display: flex;
  overflow: hidden;
  background: #000000;
  height: 40px;
  border-radius: 20px;
  align-items: center;
  padding-left: 20px;
  padding-right: 20px;
  text-overflow: clip;
  :not(:first-child) {
    margin-top: 20px;
  }
`

const FormButtom = styled.div`
  margin-top: 20px !important;
`

export default withRouter(withFirebase(SignUp))

import React, { useEffect, useState } from 'react'
import { ApolloProvider } from '@apollo/react-hooks'
import createClient from './libs/createClient'
import styled from 'styled-components'
import { Switch, Route, Redirect } from 'react-router-dom'
import withFirebase from './hocs/withFirebase'
import gql from  'graphql-tag'
import { useQuery } from '@apollo/react-hooks'
import { useGlobalState } from './libs/state'
import { isAdmin } from './libs'

import Header from './components/Header'
import Navigation from './components/Navigation'
import Landing from './pages/Landing'
import Shop from './pages/Shop'
import SignIn from './pages/SignIn'
import SignUp from './pages/SignUp'
import Loader from './components/Loader'
import ItemForm from './pages/ItemForm'
import NotFound from './pages/NotFound'
import NotAuthorized from './pages/NotAuthorized'
import Bookings from './pages/Bookings'

const FETCH_USER = gql`
  query fetchUser($email: String) {
    user(where: { email: { _eq: $email } }) {
      id
      email
      first_name
      last_name
      role
    }
  }
`

const App = ({ firebase }) => {
  const [authState, setAuthState] = useState({ loading: true })

  useEffect(() => {
    return firebase.auth.onAuthStateChanged(async user => {
      if (user) {
        const token = await user.getIdToken()
        const idTokenResult = await user.getIdTokenResult()
        const hasuraClaim = idTokenResult.claims['https://hasura.io/jwt/claims']

        if (hasuraClaim) {
          firebase.database().ref('metadata/' + user.uid).once('value')
            .then(snapshot => {
              const role = snapshot.val() && snapshot.val().role || 'user'
              setAuthState({ user, token, role })
            })
        } else {
          // Check if refresh is required.
          const metadataRef = firebase
            .database()
            .ref('metadata/' + user.uid + '/refreshTime')

          metadataRef.on('value', async () => {
            // Force refresh to pick up the latest custom claims changes.
            const token = await user.getIdToken(true)
            firebase.database().ref('metadata/' + user.uid).once('value')
              .then(snapshot => {
                const role = snapshot.val().role || 'user'
                setAuthState({ user, token, role })
              })
          })
        }
      } else {
        setAuthState({ user: null, token: null })
      }
    })
  }, [firebase])

  return (
    <ApolloProvider client={createClient({ token: authState.token, role: authState.role })}>
      <Wrapper 
        mobile={window.orientation !== undefined} // check if on mobile 
        height={window.innerHeight}
        width={window.innerWidth}
      >
        {authState.loading ? <Loader /> : <Main authState={authState} />}
      </Wrapper>
    </ApolloProvider>
  )
}

const Main = ({ authState = { user: { first_name: "Joel" } } }) => {
  const { data, loading } = useQuery(FETCH_USER, {
    variables: { email: authState.user && authState.user.email },
  })
  const user = authState.user ? data && data.user && data.user[0] : null
  const [, globalDispatch] = useGlobalState()

  useEffect(() => {
    globalDispatch({
      currentUser: user
    })
  }, [user, globalDispatch])

  return (
    <>
      <Header />
      <MainWrapper>
        {
          loading ? 
          <Loader /> : 
          <Switch>
            <Route exact path='/' component={Landing} />
            <Route exact path='/shop' component={Shop} />
            <Route exact path='/sign-in' component={SignIn} />
            <Route exact path='/sign-up' component={SignUp} />
            <Route exact path='/401' component={NotAuthorized} />
            <Route exact path='/bookings' render={() => (
              isAdmin(user) ? <Bookings /> : <Redirect to='/401' />
            )} />
            <Route exact path='/new-item' render={() => (
              isAdmin(user) ? <ItemForm /> : <Redirect to='/401' />
            )} />
            <Route exact path='/edit-item' render={() => (
              isAdmin(user) ? <ItemForm /> : <Redirect to='/401' />
            )} />
            <Route component={NotFound} />
          </Switch>
        }
      </MainWrapper>
      <Navigation user={user} />
    </>
  )
}
 
const MainWrapper = styled.div`
  grid-area:  main;
  position: relative;
  display: flex;
  overflow: auto;
`

const Wrapper = styled.div`
  overflow: hidden;
  display: grid;
  grid-template-rows: 60px 
    calc(${({ mobile, width }) => mobile ? width + 'px' : '100%'} - 60px);
  grid-template-columns: 50% 50%;
  grid-template-areas: "header navigation" "main main";
  font-family: 'Lexend Deca', sans-serif;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: #191919;

  @media (max-width: 680px) {
    grid-template-rows: 60px 
      calc(${({ mobile, height }) => mobile ? height + 'px' : '100%'} - 120px) 60px;
    grid-template-columns: 100%;
    grid-template-areas: "header" "main" "navigation";
  } 
`

export default withFirebase(App)
